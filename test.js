
function bruteForceRecursive(currentPath, nodesLeft) {
    if(nodesLeft.length == 0) {
        pathLength = 0;
        for(i = 0; i < currentPath.length - 1; i++) {
            pathLength += distance(currentPath[i], currentPath[i+1]);
        }
        // [Austin, Dallas, Houston, 18]
        return currentPath.append(pathLength);
    }
    curSolution = [Number.MAX_VALUE];
    for(i = 0; i < nodesLeft.length; i++) {
        newCurrentPath = currentPath;
        newCurrentPath.push(nodesLeft[i]);
        newNodesLeft = nodesLeft;
        newNodesLeft.splice(i, 1);
        newSolution = bruteForceRecursive(newCurrentPath, newNodesLeft);
        if(newSolution[newSolution.length - 1] < curSolution[curSolution.length - 1]) {
            curSolution = newSolution;
        }
        return curSolution;
    }
}

console.log(bruteForceRecursive([], [[3,0],[0,4],[3,4]]))