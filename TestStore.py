'''
This is a script to perform unit testing on each function of Store.py,
tested by coverage3, so it won't got terminated with one error. 
'''
from io       import StringIO
from unittest import main, TestCase

from Store import Store_read,getDis,Store_dis
# ------------
# TestStore
# ------------

class TestStore (TestCase):

    def test_Store_read_1 (self):
        org = "us,mendoza,Mendoza,TX,,30.0047222,-97.6858333"
        ename,elat,elon = "mendoza","30.0047222","-97.6858333"
        aname,alat,alon = Store_read(org)
        self.assertEqual(ename,aname)
        self.assertEqual(elat,alat)
        self.assertEqual(elon,alon)

    def test_Store_read_2 (self):
        org = "us,rio bravo,Rio Bravo,TX,,27.3638889,-99.4797222"
        ename,elat,elon = "rio bravo","27.3638889","-99.4797222"
        aname,alat,alon = Store_read(org)
        self.assertEqual(ename,aname)
        self.assertEqual(elat,alat)
        self.assertEqual(elon,alon)

    def test_Store_read_3 (self):
        org = "us,austin,Austin,TX,678368,30.2669444,-97.7427778"
        ename,elat,elon = "austin","30.2669444","-97.7427778"
        aname,alat,alon = Store_read(org)
        self.assertEqual(ename,aname)
        self.assertEqual(elat,alat)
        self.assertEqual(elon,alon)

    def test_Store_read_4 (self):
        org = "us,oates prairie,Oates Prairie,TX,,29.7966667,-95.2422222"
        ename,elat,elon = "oates prairie","29.7966667","-95.2422222"
        aname,alat,alon = Store_read(org)
        self.assertEqual(ename,aname)
        self.assertEqual(elat,alat)
        self.assertEqual(elon,alon)

    def test_hav_1 (self):
        #org = lat,lon
        bryan = (30.6741667,-96.3697222)
        waco = (31.5491667,-97.1463889)
        answer = 126
        error = answer * 0.1
        answerTail = answer + error
        answerHead = answer - error
        actual = getDis(bryan,waco)
        self.assertTrue(actual >= answerHead and actual <= answerTail)

    def test_hav_2 (self):
        #org = lat,lon
        midland = (31.9972222,-102.0775000)
        austin =  (30.2669444,-97.7427778)
        answer = 456
        error = answer * 0.1
        answerTail = answer + error
        answerHead = answer - error
        actual = getDis(midland,austin)
        self.assertTrue(actual >= answerHead and actual <= answerTail)

    def test_hav_3(self):
        dallas = (33.1397222,-96.9886111)
        hereford = (34.8150000,-102.3972222)
        answer = 566
        error = answer * 0.1
        answerTail = answer + error
        answerHead = answer - error
        actual = getDis(dallas,hereford)
        self.assertTrue(actual >= answerHead and actual <= answerTail)

    def test_hav_4(self):
        beaumont = (30.0858333,-94.1016667)
        livingston = (30.7108333,-94.9327778)
        answer = 106
        error = answer * 0.1
        answerTail = answer + error
        answerHead = answer - error
        actual = getDis(beaumont,livingston)
        self.assertTrue(actual >= answerHead and actual <= answerTail)

    def test_hav_5(self):
        irving = (32.8138889,-96.9486111)
        wichita_falls = (33.9136111,-98.4930556)
        answer = 185
        error = answer * 0.1
        answerTail = answer + error
        answerHead = answer - error
        actual = getDis(irving,wichita_falls)
        self.assertTrue(actual >= answerHead and actual <= answerTail)

    def test_hav_6(self):
        waxahachie = (32.3863889,-96.8480556)
        denison = (33.7555556,-96.5363889)
        answer = 155
        error = answer * 0.1
        answerTail = answer + error
        answerHead = answer - error
        actual = getDis(waxahachie,denison)
        self.assertTrue(actual >= answerHead and actual <= answerTail)

if __name__ == "__main__" :
    main()
#-------------------------------------------------------------------------
"""
Instruction
% coverage3 run --branch TestStore.py >  TestStore.out 2>&1

% coverage3 report -m                   >> TestStore.out

% cat TestStore.out
.......
----------------------------------------------------------------------
"""
