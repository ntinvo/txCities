import sys
'''
This will be a script to take out subset of the city based on the given list,
since it's impossible to parse out cities manaully, if there are a lot of them
'''



def SubsetCity_write (i):
    """
    print out the selected line of the city
    """
    sys.stdout.write(str(i) + "\n")

def SubsetCity_break(line):
    """
    Return the name of the city
    """
    a = line.split(',')
    return a[1]

def SubsetCity_choose(fileName):
    line = fileName.readline()    
    while line != '':
        city = SubsetCity_break(line)
        if city in city_list:
            SubsetCity_write(line)
        line = fileName.readline()

'''
Main function
'''
city_list = ["houston", "san antonio", "dallas", "austin", "fort worth"]
f = open('txcitiespop.txt', 'r')
f.readline() #Skip the first unnecessery header
SubsetCity_choose(f)
